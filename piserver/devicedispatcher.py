__license__ =  """
/*
 *   Copyright 2012 Sebastian Kuegler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
"""


class DeviceDispatcher():

    __doc__  = "DeviceDispatcher is used to trigger certain actions on a device connected to the Arduino board"

    identifier = ""
    name = ""
    description = ""
    state = False
    dimmable = False
    stateful = True
    icon = "ktip"

    def __init__(self, serialmonitor):
        self.serialmonitor = serialmonitor

    def on(self):
        #print("Switching %s ON." % self.name)
        self.state = True
        self.serialmonitor.switchDevice(self.identifier, self.state)

    def off(self):
        #print("Switching %s OFF." % self.name)
        self.state = False
        self.serialmonitor.switchDevice(self.identifier, self.state)

