__license__ =  """
/*
 *   Copyright 2012 Sebastian Kuegler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
"""

import time

from twisted.internet import reactor
from twisted.web.server import Site
from twisted.web.resource import Resource

class ServerPage(Resource):
    isLeaf = True

    devicegroups = []

    def setSerialMonitor(self, smonitor):
        self.smon = smonitor

    def render_GET(self, request):
        request.setLastModified(time.time())
        print("URL requested: " + request.path)
        if request.path == "/status" or request.path == "/json/status":
            return self.status()
        if (request.path.startswith("/json/")):
            return self.renderJSON(request)
        else:
            return self.renderHTML(request)

    def renderJSON(self, request):

        ps = request.path.split("/")[2:]
        error = ""

        if (len(ps) < 2):
            error = "Invalid Parameters"
        else:
            device = ps[0]
            action = ps[1]

            if (len(ps) > 2):
                parameters = ps[2:]

            devices = {}

            for grp in self.devicegroups:
                for d in grp.devices:
                    devices[d.identifier] = d

            if device not in devices:
                error = "Device not found: %s" % device

            if error == "":
                dev = devices[device]
                if (action.lower() == "on"):
                    dev.on()
                elif (action.lower() == "off"):
                    dev.off()
                else:
                    error = "Action not supported: %s" % action

        if error == "":
            time.sleep(0.05)
            return self.status()
        else:
            return "[\n\t{\n\t\t\"error\": \"%s\"\n\t}\n]" % error


    def renderHTML(self, request):
        if request.path == "/status" or request.path == "/json/status":
            return self.status()

        out = "<html><body>"
        out += "<h1>This is the PiServer</h1>"

        ps = request.path.split("/")[1:]

        error = ""

        if (len(ps) < 2):
            return self.help()
            #return err("No device and/or action specified")

        device = ps[0]
        action = ps[1]

        if (len(ps) > 2):
            parameters = ps[2:]

        out += "Device: <strong>" + device + "</strong><br />"
        out += "Action: <strong>" + action + "</strong><br />"
        if (len(ps) > 2):
            out += "Parameters: " + ", ".join(parameters)

        out += "<h2>Environment</h2>"
        out += "Temperature: " + self.smon.temperature
        print(out)
        devices = {}

        for grp in self.devicegroups:
            for d in grp.devices:
                devices[d.identifier] = d
        #return
        if device in devices:
            # method dispatch here
            print("doing something")
        else:
            error = err("<h1>Device Error</h1>")
            error += "Device <strong>" + device + "</strong> not found<br />"
            error += "Available devices: <ul><li>" + "</li><li>".join(self.devices.keys()) + "</li></ul>"
            return error

        # The user specified an existing device, let's roll

        dev = devices[device]

        if (action.lower() == "on"):
            dev.on()
        elif (action.lower() == "off"):
            dev.off()
        else:
            # error condition
            error = err("<h1>Action Error</h1>")
            error += "Action <strong>" + action + "</strong> not supported<br />"
            error += "Available devices: <ul><li>" + "</li><li>".join(("On", "Off")) + "</li></ul>"
            return error
        time.sleep(0.1)

        out = "<h1>Success</h1>"
        #out = "<p>" + dev.message + "</p>"

        out += self.help()

        return out

    def help(self):
        out = "<h1>Welcome to the PiArduino Server</h1>"
        out += "<p>This webserver allows you to remote control physical devices, such as switch the power to certain devices on an off.</p>"
        out += "<h2>The following devices and action are available</h2>\n"

        devices = {}

        for grp in self.devicegroups:
            for d in grp.devices:
                devices[d.identifier] = d

        #for d in devices:
            #out += "<p><ul>"

        for k in devices.keys():
            out += "<li>"
            d = devices[k]
            out += "<strong>" + d.name + "</strong>: " + d.description
            if not d.state:
                out += " <a href=\"/%s/On\">Switch ON</a>" % d.identifier
            else:
                out += " <a href=\"/%s/Off\">Switch OFF</a>" % d.identifier
            out += "</li>"
        out += "</ul>"

        out += "<h2>Environment</h2>"
        out += "Temperature: " + self.smon.temperature

        return out

    def status(self):
        __doc__ = "Generate JSON status information"
        _hc = "Home Controller"
        apiVersion = "1.0";
        out = "{\n"

        out += "\"controller\": \"" + _hc + "\",\n"
        out += "\"apiversion\": \"" + apiVersion + "\",\n"
        out += "\"devicegroups\": ["
        grps = []
        for grp in self.devicegroups:
            #grp = self.devicegroups[_g]
            o = ""
            o += "\n\t{\n"
            o += "\t\t\"groupname\": \"" + grp.name + "\",\n"
            o += "\t\t\"groupidentifier\": \"" + grp.identifier + "\",\n"
            o += "\t\t\"imageurl\": \"%s\",\n" % grp.imageurl
            _temp = ""
            if grp.name in ("Studio", "Arduino"):
                _temp = self.smon.temperature
            o += "\t\t\"temperature\": \"" + _temp + "\",\n"

            o += "\t\t\"switches\": [\n"
            lines = []
            for d in grp.devices:
                #d = grp.devices[k]
                _s = "false"
                if d.identifier in self.smon.leds.keys():
                    _s = ["false", "true"][int(self.smon.leds[d.identifier])]
                    #print("%s == %s" % (k, _s))
                l = "\t\t\t{\n"
                keys = []
                keys.append("\t\t\t\t\"identifier\": \"%s\"" % d.identifier)
                keys.append("\t\t\t\t\"name\": \"%s\"" % d.name)
                keys.append("\t\t\t\t\"description\": \"%s\"" % d.description)
                keys.append("\t\t\t\t\"dimmable\": %s" % ["false", "true"][d.dimmable])
                keys.append("\t\t\t\t\"state\": %s" % ["false", "true"][d.state])
                keys.append("\t\t\t\t\"stateful\": %s" % ["false", "true"][d.stateful])
                keys.append("\t\t\t\t\"icon\": \"%s\"" % d.icon)
                l += ",\n".join(keys) + "\n"
                l += "\t\t\t}"
                lines.append(l)

            o += ",\n".join(lines) + "\n"
            #o += "\t}\n"
            o += "\t\t]\n\t}"
            grps.append(o)
        out += ",\t".join(grps) + "\n"

        out += "\n]\n}\n"
        #print(out)
        return out

def err(msg):
    return "<font color=\"red\">%s</font>" % msg

