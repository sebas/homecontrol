__license__ =  """
/*
 *   Copyright 2013 Sebastian Kuegler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
"""

import os
from devicedispatcher import *

class CommandDispatcher(DeviceDispatcher):

    __doc__  = """CommandDispatcher is used to run a command on the server. 
    Beware, this happens as root."""

    state = False
    icon = "system-run"
    oncommand = "uname -a"
    offcommand = "uptime"
    ondescription = ""
    offdescription = ""

    __defaultdescription = ""

    def __init__(self, serialmonitor):
        return

    def on(self):
        print("On: Running %s" % self.oncommand)
        os.system(self.oncommand)
        self.state = True
        self.updateState()

    def off(self):
        print("Off: Running %s" % self.offcommand)
        os.system(self.offcommand)
        self.state = False
        self.updateState()

    def updateState(self):
        if self.__defaultdescription == "":
            self.__defaultdescription = self.description
        if self.state == True:
            self.description = self.ondescription
        else:
            self.description = self.offdescription
        if self.description == "" and self.__defaultdescription != "":
            self.description = self.__defaultdescription
        print("Set description to " + self.description + " state: " + str(self.state))