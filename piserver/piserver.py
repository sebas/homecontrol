#!/usr/bin/env python

__license__ =  """
/*
 *   Copyright 2012 Sebastian Kuegler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
"""

"""
Serial protocol:
* uppercase means on
* lowercase means off

The following chars are handled:

A / a : pluggable dimmer
B / b : spots above desk
C / c : rooftop lamp
D / d : background lights
E     : bell

K / l : ceiling lamp living room
L     : bell

X / x : red LED
Y / y : white LED
Z / z : blue LED

"""

from twisted.web.server import Site
from twisted.python import log

import time
import sys

from serialmonitor import *
from commanddispatcher import *
from devicedispatcher import *
from devicegroup import *
from serverpage import *

log.startLogging(sys.stdout)

serialmonitor = SerialMonitor();
serialmonitor.start()


# Build Tree of devices, sorted into groups

# Studio
studio = DeviceGroup("Studio")
studio.identifier = "S"
studio.imageurl = "http://192.168.1.31/homecontroller/images/studio.jpg"

wakemiro = CommandDispatcher(serialmonitor)
wakemiro.name = "Miro"
wakemiro.icon = "computer"
wakemiro.ondescription = "Workstation is running"
wakemiro.offdescription = "Workstation is sleeping"
wakemiro.identifier = "W"
wakemiro.oncommand = "/home/sebas/bin/wake-miro"
wakemiro.offcommand = "/home/sebas/bin/sleep-miro"
wakemiro.updateState()

deskspots = DeviceDispatcher(serialmonitor)
deskspots.name = "Desk"
deskspots.description = "Spots above desk"
deskspots.dimmable = True
deskspots.identifier = "B"

rooftop = DeviceDispatcher(serialmonitor)
rooftop.name = "Main Light"
rooftop.description = "Lamps under rooftop"
rooftop.dimmable = True
rooftop.identifier = "C"

backgroundlight = DeviceDispatcher(serialmonitor)
backgroundlight.name = "Background Light"
backgroundlight.description = "Table Lamps in the corners"
backgroundlight.dimmable = True
backgroundlight.identifier = "D"

bellAttic = DeviceDispatcher(serialmonitor)
bellAttic.name = "Bell"
bellAttic.description = "in attic"
bellAttic.dimmable = False
bellAttic.identifier = "E"
bellAttic.stateful = False
bellAttic.icon = "preferences-desktop-notification-bell"

studio.devices = [wakemiro, deskspots, rooftop, backgroundlight, bellAttic]

# Living Room
livingroom = DeviceGroup("Ground Floor")
livingroom.identifier = "L"
livingroom.imageurl = "http://192.168.1.31/homecontroller/images/livingroom.jpg"

ceilingLamp = DeviceDispatcher(serialmonitor)
ceilingLamp.name = "Ceiling Spots"
ceilingLamp.description = "Main Light"
ceilingLamp.dimmable = True
ceilingLamp.identifier = "K"

dimmer = DeviceDispatcher(serialmonitor)
dimmer.name = "Table Lamp"
dimmer.description = "Beneath CDs"
dimmer.dimmable = True
dimmer.identifier = "A"

gardenLamp = DeviceDispatcher(serialmonitor)
gardenLamp.name = "Garden"
gardenLamp.description = "Lamp in the garden"
gardenLamp.dimmable = False
gardenLamp.identifier = "M"

plugSwitch2 = DeviceDispatcher(serialmonitor)
plugSwitch2.name = "Pluggable Switch 2"
plugSwitch2.description = ""
plugSwitch2.dimmable = False
plugSwitch2.identifier = "N"

plugSwitch3 = DeviceDispatcher(serialmonitor)
plugSwitch3.name = "Pluggable Switch 3"
plugSwitch3.description = ""
plugSwitch3.dimmable = False
plugSwitch3.identifier = "O"

bellGround = DeviceDispatcher(serialmonitor)
bellGround.name = "Bell"
bellGround.description = "in the living room"
bellGround.dimmable = False
bellGround.identifier = "L"
bellGround.icon = "preferences-desktop-notification-bell"
bellGround.stateful = False

livingroom.devices = [ceilingLamp, dimmer, gardenLamp, plugSwitch2, plugSwitch3, bellGround]

# Arduino LEDs
arduino = DeviceGroup("Arduino")
arduino.identifier = "A"
arduino.imageurl = "http://192.168.1.31/homecontroller/images/arduino.jpg"

redled = DeviceDispatcher(serialmonitor)
redled.name = "Red LED"
redled.description = "on the Arduino board"
redled.dimmable = False
redled.state = True
redled.identifier = "X"

whiteled = DeviceDispatcher(serialmonitor)
whiteled.name = "White LED"
whiteled.description = "on the Arduino board"
whiteled.dimmable = False
whiteled.state = True
whiteled.identifier = "Y"

blueled = DeviceDispatcher(serialmonitor)
blueled.name = "Blue LED"
blueled.description = "on the Arduino board"
blueled.dimmable = False
blueled.state = True
blueled.identifier = "Z"

arduino.devices = [redled, whiteled, blueled]



# Create server, make devicegroups known to it

serverpage = ServerPage()
serverpage.setSerialMonitor(serialmonitor)
serverpage.devicegroups = [studio, livingroom, arduino]

# Start the server
factory = Site(serverpage)
reactor.listenTCP(88, factory)
reactor.run()