__license__ =  """
/*
 *   Copyright 2012 Sebastian Kuegler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
"""

import threading
import time
import serial
import os.path

class SerialMonitor(threading.Thread):

    ports = ("/dev/ttyACM0", "/dev/ttyACM1", "/dev/ttyACM2", "/dev/ttyACM3")
    port = ports[0]

    temperature = "37.7"

    leds = {}
    serialdata = {}

    def __init__(self):
        threading.Thread.__init__(self)
        #print("Thread init'ed")#self.out_queue = out_quep
        self.findSerialPort()

    def findSerialPort(self):
        port_exists = False
        for p in self.ports:
            if os.path.exists(p):
                if self.connectSerialPort(p): return

    def connectSerialPort(self, port):
        try:
            self.ser = ser = serial.Serial(port, 9600, timeout=3)
            #self.ser.open()
            print("Serial Port opened: " + port)
            self.port = port
            return True
        except serial.serialutil.SerialException:
            #print("Error opening serial port " + port)
            return False

    def switchDevice(self, identifier, newstate):
        print("switching Device %s %s" % (identifier, newstate) )
        try:
            if newstate:
                val = identifier.upper()
            else:
                val = identifier.lower()
            print(" +++ Writing val to serial port: " + val)
            self.ser.write(val)
            self.ser.flush()
            self.leds[identifier.upper()] = ["0", "1"][newstate]

            #self.ser.close()
        except (AttributeError, OSError, serial.serialutil.SerialException):
            #print("serial port invalid")
            self.findSerialPort()
            time.sleep(.1)
        print(self.leds)

    def parseSerialLine(self, line):
        """
        DATA:T1=43.78;A=0;B=1;C=0;K=1;

        """
        #print("Line: " + line)
        if not line.startswith("DATA:"):
            #print("off: " + line)
            return
        line = line.replace("DATA:", "").strip()
        #print line.split(";")
        for token in line.split(";"):
            pcs = token.split("=")
            k = pcs[0]
            #print(pcs)
            if len(pcs) == 2 and k != "":
                self.serialdata[pcs[0]] = pcs[1]
                if k not in ("T1"):
                    self.leds[k] = pcs[1]
        #print self.leds
        #print self.serialdata
        #temp = line.split("C: ")[1].strip().split(";")[0]
        #volts = line.split(",")[1].split(": ")[1]
        self.temperature = self.serialdata["T1"]
        #print(self.leds)


    def run(self):
        #return
        i = 21
        while True:
            #print("Thread run")
            time.sleep(.5)
            try:
                #self.ser.open()
                #s = self.ser.read(64)
                line = self.ser.readline()
                if (line.startswith("DATA:") and line.endswith("\r\n")):
                    #print("OK: " + line);
                    try:
                        self.parseSerialLine(line)
                        if i > 4:
                            print(" OK temp: " + self.temperature + " " + line[:-1]);
                            i = 0
                        i = i+1
                    except IndexError:
                        continue
                        #print("IndexError Incomplete line: " + line)
                elif line.startswith("LOG:"):
                    print(line[:-1])
                else:
                    #print("Incomplete line: " + line[:-1])
                    i = 10 # trigger print
                #self.ser.close()
            except (AttributeError, OSError, serial.serialutil.SerialException):
                #print("serial port invalid")
                self.findSerialPort()
                time.sleep(.1)
                #continue
