/*
 *   Copyright 2012 Sebastian K gler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.2
//import QtQuick.Layouts 1.1


import "parser.js" as JS

Item {
    //id:root

    width: 400
    height: 600

    property string url: "http://localhost:88/" // FIXME: read / write from config

    property string controller: ""
    property string apiversion: ""
    property bool showUrl: false
    property bool showSettings: false
    property string error: ""
    property int seconds: 0

    property alias groupModel: groupModel
    property alias listModel: listModel
    property alias switchModel: switchModel

    property string jsonData: ""
    property string currentGroup: "S" // FIXME: read from config
    property int currentGroupIndex: 0

    property bool skipGroupReset: false
    property bool useCache: false


    function reload() {
        print("now...");
        print("root.url: " + root.url + " " + root.url.length);
        var l = root.url.lastIndexOf("/");
        print ("L : " + l);
        print(" l i : " + l +  " " + root.url.length-1);
        var u = root.url;
        if (!(l == root.url.length-1)) {
            u = u + "/";
        }
        print("load." + u + "status");
        JS.load(u + "status")
    }

    ListModel {
        id: listModel
    }

    ListModel {
        id: groupModel
    }

    ListModel {
        id: switchModel
    }

    Timer {
        id: reloadTimer
        repeat: true
        running: false
        interval: seconds*1000
        onTriggered: reload();
    }
    /*
    GroupView {
        id: groupView
        height: 120
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }
    }

    ListView {
        id: view
        spacing: 12
        model: listModel
        opacity: root.showSettings ? 0.1 : 1
        clip: true
        interactive: contentHeight > height
        //model: JS.switchModel["S"]
        anchors {
            left: parent.left
            top: groupView.bottom
            right: parent.right
            bottom: parent.bottom
            margins: 12

        }

        Behavior on opacity { PropertyAnimation {} }

        delegate: SwitchDelegate {}
    }

    Settings {
        anchors.fill: parent
        anchors.margins: 12
    }

    PlasmaComponents.ToolButton {
        id: refreshButton
        iconSource: !root.showSettings ? "configure" : "dialog-ok-apply"
        enabled: !root.showSettings || (root.error == "")
        //opacity: root.showSettings ? 0 : 1
        anchors {
            top: parent.top
            right: parent.right
            margins: 12

        }
        onClicked: root.showSettings = !root.showSettings

        Behavior on opacity { PropertyAnimation {} }
    }
    */
    function loadConfig() {
        print("generic config loader !");
        reload();
    }

    Component.onCompleted: {
        reload();
    }
}
