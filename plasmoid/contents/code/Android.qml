/*
 *   Copyright 2012 Sebastian K�gler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.2
import QtQuick.Window 2.1
import QtQuick.Controls 1.2

import "parser.js" as JS


ApplicationWindow {
    id: rootWindow

    property bool isLandscape: width > height
    property int _margins: Math.min(width, height) / 8

    visible: true
    width: 720
    height: 360

    title: qsTr("Home Controller")

    menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("Components")
                onTriggered: loadPage("ComponentsTab")
            }
            MenuItem {
                text: qsTr("Geolocation")
                onTriggered: loadPage("GeoTab")
            }
            MenuItem {
                text: qsTr("Sensors")
                onTriggered: loadPage("SensorTab")
            }
            MenuItem {
                text: qsTr("OpenGL Shader")
                onTriggered: loadPage("ColorShower")
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit()
            }
        }
    }

    HomeController {
        id: root

        url: "http://192.168.1.4:88/"
        
        property alias groupView: groupView

        anchors.fill: parent

        Rectangle {
            anchors.fill: parent
            color: "orange"
        }

        GroupView {
            id: groupView
            height: 120
            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
            }
        }


        ListView {
            id: view
            spacing: 12
            model: root.listModel
            opacity: root.showSettings ? 0.1 : 1
            clip: true
            interactive: contentHeight > height
            //model: JS.switchModel["S"]
            anchors {
                left: parent.left
                top: groupView.bottom
                right: parent.right
                bottom: parent.bottom
                margins: 12

            }

            Behavior on opacity { PropertyAnimation {} }

            //delegate: SwitchDelegate {}
            delegate: Item {
                width: parent.width
                height: childrenRect.height

                Text {
                    id: dname
                    text: devicename
                    anchors {
                        left: parent.left
                        right: parent.right
                        top: parent.top
                    }
                }
                Text {
                    text: devicedescription
                    anchors {
                        left: parent.left
                        right: parent.right
                        top: dname.bottom
                    }
                }
            }
        }

        function loadConfig() {
            print("Android specific config CONFIG ChANGED!");
            root.url = "http://192.168.1.4:88/";
            reload();
        }

        Component.onCompleted: {
    //         //plasmoid.aspectRatioMode = IgnoreAspectRatio;
    // //         plasmoid.addEventListener('configChanged', function() {
    // //             loadConfig();
    // //         })
            root.url = "http://192.168.1.4:88/";
            reload();
        }

    }
}
