/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import "parser.js" as JS

Item {
    id: settingsItem

    width: 200
    height: 300

    opacity: root.showSettings ? 1 : 0

    Behavior on opacity { PropertyAnimation {} }

    PlasmaExtras.Title {
        id: settingsTitle
        anchors {
            top: parent.top
            left: parent.left
            right: parent.right
            margins: 12
        }
        text: i18n("Home Controller")
    }

    PlasmaExtras.Paragraph {
        id: infoText

        anchors {
            top: settingsTitle.bottom
            left: parent.left
            right: parent.right
            margins: 12
        }

        text: {
            var o = ""
            if (root.error == "") {
                o += i18n("Connected to %1: ", root.controller);
                var i = root.groupModel.count;
                o += i18n("API version: %1, %2 groups available.", root.apiversion, i);
            } else {
                o = root.error;
            }
            o += "<br /><br />Controller URL:";
            return o;
        }
    }

    PlasmaComponents.TextField {
        id: urlField
        text: root.url
        Keys.onEnterPressed: {
            print("reloading()");
            reload();
        }
        onTextChanged: {
            root.url = text;
            loadTimer.running = true;
        }
        anchors {
            top: infoText.bottom
            left: parent.left
            right: refreshButton.left
            margins: 12
        }
        Timer {
            id: loadTimer
            interval: 1000
            running: false
            repeat: false
            onTriggered: reload();
        }
        Behavior on opacity { PropertyAnimation {} }
    }
    PlasmaComponents.ToolButton {
        id: refreshButton
        iconSource: "view-refresh"
        anchors {
            right: parent.right
            verticalCenter: urlField.verticalCenter
            margins: 12
        }
        onClicked: {
            plasmoid.writeConfig("url", root.url);
            reload();
        }

        Behavior on opacity { PropertyAnimation {} }
    }

    PlasmaComponents.Label {
        id: autoreloadlabel
        text: root.seconds == 0 ? i18n("Do not reload automatically") : i18n("Reload every %1 seconds", root.seconds)
        anchors {
            top: urlField.bottom
            left: parent.left
            right: parent.right
            margins: 12
        }
    }

    PlasmaComponents.Slider {
        id: reloadSlider
        minimumValue: 0
        maximumValue: 10
        onValueChanged: {
            var s = value * value * value;
            root.seconds = s;
            reloadTimer.running = (root.seconds > 0);
        }
        anchors {
            top: autoreloadlabel.bottom
            left: parent.left
            right: parent.right
            margins: 12
        }
    }

    //Rectangle { color: "grey"; opacity: 0.3; anchors.fill: parent; }
}
