/*
 *   Copyright 2012 Sebastian K�gler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.2
import QtQuick.Layouts 1.1

import org.kde.plasma.plasmoid 2.0
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import "parser.js" as JS

HomeController {
    id: root

    width: units.gridUnit * 26
    height: units.gridUnit * 20

    Plasmoid.switchWidth: units.gridUnit * 10
    Plasmoid.switchHeight: units.gridUnit * 8


    GroupView {
        id: groupView
        height: 120
        anchors {
            left: parent.left
            right: parent.right
            top: parent.top
        }
    }

    ListView {
        id: view
        spacing: 12
        model: root.listModel
        opacity: root.showSettings ? 0.1 : 1
        clip: true
        interactive: contentHeight > height
        //model: JS.switchModel["S"]
        anchors {
            left: parent.left
            top: groupView.bottom
            right: parent.right
            bottom: parent.bottom
            margins: 12

        }

        Behavior on opacity { PropertyAnimation {} }

        delegate: SwitchDelegate {}
    }

    Settings {
        anchors.fill: parent
        anchors.margins: 12
    }

    PlasmaComponents.ToolButton {
        id: refreshButton
        iconSource: !root.showSettings ? "configure" : "dialog-ok-apply"
        enabled: !root.showSettings || (root.error == "")
        //opacity: root.showSettings ? 0 : 1
        anchors {
            top: parent.top
            right: parent.right
            margins: 12

        }
        onClicked: root.showSettings = !root.showSettings

        Behavior on opacity { PropertyAnimation {} }
    }

    function loadConfig() {
        print("Plasmoid specific config CONFIG ChANGED!");
        var u = plasmoid.readConfig("url");
        var s = plasmoid.readConfig("reloadInterval");
        print(" read settings: ");
        if (typeof(u) != "undefined" && u) {
            print("     url = " + u);
            root.url = u;
        }
        if (typeof(s) != "undefined" && s) {
            print("     reloadInterval = " + s);
            root.seconds = s;
        }
        reload();
    }

    Component.onCompleted: {
        //plasmoid.aspectRatioMode = IgnoreAspectRatio;
//         plasmoid.addEventListener('configChanged', function() {
//             loadConfig();
//         })
        reload();
    }

}
