/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.2
import org.kde.plasma.core 2.0 as PlasmaCore
import org.kde.plasma.components 2.0 as PlasmaComponents

import "parser.js" as JS

Item {
    id: switchDelegate
    width:parent.width
    height:64
    PlasmaComponents.Button {
        anchors.fill: parent
        checked: devicestate
        checkable: stateful
        PlasmaComponents.Label {
            id: dname
            text: devicename
            elide: Text.ElideRight
            anchors {
                leftMargin: 12
                left: bulb.right
                bottom: parent.verticalCenter
                right: parent.right
            }

        }
        PlasmaComponents.Label {
            id: ddesc
            text: devicedescription
            elide: Text.ElideRight
            font.pointSize: theme.smallestFont.pointSize
            opacity: 0.4
            anchors {
                leftMargin: 12
                left: bulb.right
                top: dname.bottom
                right: parent.right
            }
        }
        PlasmaCore.IconItem {
            id: bulb
            source: "ktip"
            //iconSource: deviceicon
            height: 36
            width: 36
            enabled: !stateful || parent.checked
            anchors {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: 12
            }
        }
        PlasmaComponents.ToolButton {
            text: "Dim"
            width: parent.height
            height: parent.height / 2
            visible: (dimmable && devicestate)
            checkable: false
            anchors {
                verticalCenter: parent.verticalCenter
                right: parent.right
                rightMargin: parent.height / 4
            }
            onClicked: {
                var l = root.url.lastIndexOf("/");
                var u = root.url;
                if (!(l == root.url.length-1)) {
                    u = u + "/";
                }
                root.skipGroupReset = true;
                var url = u + "json/" + deviceidentifier + "/On";
                print(" Calling url: " + url);
                JS.aswitch(url);
            }
            Behavior on opacity { PropertyAnimation {} }
        }
        onClicked: {
            var l = root.url.lastIndexOf("/");
            var u = root.url;
            if (!(l == root.url.length-1)) {
                u = u + "/";
            }

            var s = devicestate ? "Off" : "On"
            root.skipGroupReset = true;
            var url = u + "json/" + deviceidentifier + "/" + s;
            JS.aswitch(url);
        }
    }
}
