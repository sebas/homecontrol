/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

import QtQuick 2.2
//import org.kde.plasma.core 2.0 as PlasmaCore
// import org.kde.plasma.components 2.0 as PlasmaComponents
// import org.kde.plasma.extras 2.0 as PlasmaExtras

import "parser.js" as JS


ListView {
    id: groupView
    model: root.groupModel
    //height: contentHeight

    clip: true
    orientation: ListView.Horizontal
    opacity: root.showSettings ? 0.1 : 1
    snapMode: ListView.SnapOneItem
    boundsBehavior: Flickable.DragOverBounds

    Behavior on opacity { PropertyAnimation {} }

    onFlickEnded: {
        var ix = contentX == 0 ? 0 : (contentX/parent.width);
        var gid = root.groupModel.get(ix)["groupidentifier"];
        print("Switching group to : " + gid + " ix: " + ix);
        JS.changeSwitchesModel(gid, ix);
        root.skipGroupReset = true;
        root.useCache = true;
        root.reload();
    }

    delegate: Item {
        height: 120
        width: groupView.width


        Image {
            id: groupimage
            source: imageurl
            fillMode: Image.PreserveAspectFit
            anchors {
                top: title.bottom
                right: parent.right
                rightMargin: 12
                bottom: parent.bottom
            }

        }

        //PlasmaExtras.Title {
        Text {
            font.pointSize: 20
            color: "white"
            id: title
            text: groupname
            opacity: 0.6
            anchors {
                left: parent.left
                //top: parent.top
                top: parent.top
                right: parent.right
                margins: 12
                //bottomMargin: 0
            }
            MouseArea {
                anchors.fill: parent
                onClicked: reload();
            }
        }

        //PlasmaComponents.Label {
        Text {
            id: temperatureItem
            font.pointSize: title.font.pointSize * 2
            opacity: 0.5
            anchors {
                right: groupimage.left
                bottom: groupimage.bottom;
                //left: left.right
                //margins: 12
                rightMargin: 24
                bottomMargin: -12
            }
            text: temperature != "" ? temperature.slice(0,-1) + "\u00B0" : ""
        }
    }
}
