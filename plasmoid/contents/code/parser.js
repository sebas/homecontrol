/*
 *   Copyright 2012 Sebastian Kügler <sebas@kde.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU Library General Public License as
 *   published by the Free Software Foundation; either version 2 or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details
 *
 *   You should have received a copy of the GNU Library General Public
 *   License along with this program; if not, write to the
 *   Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

function i18n(txt) {
    return txt;
}

function load(url) {
    // Load status information
    var xhr = new XMLHttpRequest;
    xhr.open("GET", url, true);
    try {
        var a = xhr.statusText;
    } catch (e) {
        if (e.code == DOMException.INVALID_STATE_ERR)
            print("Exception unhandled");
    }
    
    print("Loading " + url + " ....");
    if (root.useCache && root.jsonData != "") {
        print("yay>" + jsonData);
        var jsonObject = eval('(' + jsonData + ')');
        loaded(jsonObject)
        root.useCache = false;
        return;
    }
    xhr.onreadystatechange = function() {
        print("readyState: " + xhr.readyState);
        if (xhr.readyState == XMLHttpRequest.DONE) {
            if (xhr.status == 200) {
                //print(xhr.responseText);
                // Cache response
                root.jsonData = xhr.responseText;
                var jsonObject = eval('(' + xhr.responseText + ')');
                loaded(jsonObject)
            } else {
                print("load XmlHttpReq failed: " + xhr.status)
                root.error = i18n("<font size=+2>%1</font> Status request failed.", xhr.status);
                root.showSettings = true;
            }
        }
    }
    xhr.send();
}

function aswitch(url) {
    // called from buttons, sets switch and returns status information
    print("switching " + url);
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function() {
        if ( xhr.readyState == XMLHttpRequest.DONE) {
            if ( xhr.status == 200) {
                root.jsonData = xhr.responseText;
                var jsonObject = eval('(' + xhr.responseText + ')');
                loaded(jsonObject)
            } else {
                print("aswtich: XmlHttpReq aswitch failed: " + xhr.status)
                root.error = i18n("<font size=+2>%1</font> Switch request failed.", xhr.status);
                root.showSettings = true;
            }
        }
    }
    xhr.send();
}

function loaded(jsonObject) {
    // The JSON status information has successfully loaded
    print("HTTP REsponse");
    if (jsonObject[0] && jsonObject[0]["error"]) {
        print("Error : " + jsonObject[0]["error"]);
        root.error = jsonObject[0]["error"];
        root.showSettings = true;
        return;
    }
    //grpcnt = jsonObject["devicegroups"].length;
    root.controller = jsonObject["controller"];
    root.apiversion = jsonObject["apiversion"];

    listModel.clear();
    if (!root.skipGroupReset) {
        groupModel.clear();
    }
    for (var grpindex in jsonObject["devicegroups"]) {
        var groupObject = jsonObject["devicegroups"][grpindex]
        var gid = groupObject["groupidentifier"];

        if (!root.skipGroupReset) {
            var go = parseGroup(groupObject);
            groupModel.append(go);
        }

        if (gid == currentGroup) {
            for (var index in groupObject["switches"]) {
                var switchObject = parseSwitch(groupObject["switches"][index]);
                listModel.append(switchObject);
            }
        }
    }
    groupView.currentIndex = root.currentGroupIndex;
    print("Status loaded" + (root.useCache ? " from cache" : "") + ".");
    root.skipGroupReset = false;
    root.showSettings = false;
    root.error = "";
}

function parseGroup(groupObject) {
    // parse and individual switch object
    var go = {
        "groupname" : groupObject["groupname"],
        "groupidentifier" : groupObject["groupidentifier"],
        "temperature" : groupObject["temperature"],
        "imageurl" : groupObject["imageurl"],
        "switchcount" : groupObject["switches"].length
    }
    return go;
}

function parseSwitch(switchObject) {
    // parse and individual switch object
    var sw = {
        "devicename" : switchObject["name"],
        "dimmable" : switchObject["dimmable"],
        "devicedescription" : switchObject["description"],
        "devicestate" : switchObject["state"],
        "deviceidentifier" : switchObject["identifier"],
        "stateful" : switchObject["stateful"],
        "deviceicon" : switchObject["icon"]
    }
    return sw;
}


function changeSwitchesModel(groupidentifier, ix) {
    if (groupidentifier && typeof(groupidentifier != "undefined")) {
        root.currentGroup = groupidentifier;
        root.currentGroupIndex = ix;
    }
}
