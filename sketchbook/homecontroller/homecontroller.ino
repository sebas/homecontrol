/**
 *
 * Copyright 2012-2014 Sebastian Kügler <sebas@kde.org>
 *
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **/

/*

KaKu codes:

* address addres (dial switches in remote). Range [A..P] (case sensitive!) : used as receiver
* group	Group to switch. Range: [1..4] : unused
* device Device to switch. Range: [1..4] : Used as location


device:
  1 : upstairs
    address : 
      A : Pluggable dimmer 0
      B : Desk-side spots
      C : Rooftop spots
      D : Background lighting
      E : Bell

  2 : downstairs
    address : 
      A : Ceiling lamp
      B : Bell
      C : Pluggable switch 1
      D : Pluggable switch 2
      E : Pluggable switch 3

(T indicates not paired yet)

*/

/*
Serial input:
* uppercase means on
* lowercase means off

The following chars are handled:

A / a : pluggable dimmer
B / b : spots above desk
C / c : rooftop lamp
D / d : background lights
E     : bell

K / l : ceiling lamp living room
L     : bell
M / m : pluggable switch 1
N / n : pluggable switch 2
O / o : pluggable switch 3

X / x : red LED
Y / y : white LED
Z / z : blue LED

*/

#include <RemoteTransmitter.h>

// Sensor pin and offset
int sensorPin = A0;
const float baselineTemp = 20.0;

// The Klik-aan-klik-uit remote switches
bool valueA = LOW;
bool valueB = LOW;
bool valueC = LOW;
bool valueD = LOW;
bool valueE = LOW;
bool valueK = LOW;
bool valueL = LOW;
bool valueM = LOW;
bool valueN = LOW;
bool valueO = LOW;

// LED pins
int pinX = 5;
int pinY = 4;
int pinZ = 3;

int pinStatus = 13;

bool valueX = LOW;
bool valueY = LOW;
bool valueZ = LOW;

int message = 0;

// initialize counter very high, so we trigger the first update immediately
int ctr = 1000;

// Intantiate a new KaKuTransmitter remote, also use pin 11
KaKuTransmitter kaKuTransmitter(11);


void setup() {
    Serial.begin(9600);

    // Initialize our LEDs
    pinMode(pinX, OUTPUT);
    digitalWrite(pinX, valueX);

    pinMode(pinY, OUTPUT);
    digitalWrite(pinY, valueY);

    pinMode(pinZ, OUTPUT);
    digitalWrite(pinZ, valueZ);

    // green LED
    pinMode(pinStatus, OUTPUT);
    digitalWrite(pinStatus, LOW);

    blinkWhite();
}

void loop() {
    bool update = false;
    if (Serial.available()) {
        message = Serial.read();

        Serial.print("LOG:LED switched ");
        String led = "";
        if (message == 'A') {
            valueA = HIGH;
            led = "Red=1";
            kaKuTransmitter.sendSignal('A', 1, true);
        } else if (message == 'a') {
            //digitalWrite(pinX, LOW);
            valueA = LOW;
            led = "Red=0";
            kaKuTransmitter.sendSignal('A', 1, false);
        } else if (message == 'B') {
            //digitalWrite(pinY, HIGH);
            valueB = HIGH;
            led = "Green=1";
            kaKuTransmitter.sendSignal('B', 1, true);
        } else if (message == 'b') {
            //digitalWrite(pinY, LOW);
            valueB = LOW;
            led = "Green=0";
            kaKuTransmitter.sendSignal('B', 1, false);
        } else if (message == 'C') {
            //digitalWrite(pinZ, HIGH);
            valueC = HIGH;
            led = "Blue=1";
            kaKuTransmitter.sendSignal('C', 1, true);
        } else if (message == 'c') {
            //digitalWrite(pinZ, LOW);
            valueC = LOW;
            led = "Blue=0";
            kaKuTransmitter.sendSignal('C', 1, false);
        } else if (message == 'D') {
            valueD = HIGH;
            led = "D=1";
            kaKuTransmitter.sendSignal('D', 1, true);
        } else if (message == 'd') {
            valueD = LOW;
            led = "D=0";
            kaKuTransmitter.sendSignal('D', 1, false);
        } else if (message == 'E') {
            valueE = HIGH;
            led = "E=1";
            kaKuTransmitter.sendSignal('E', 1, true);
        } else if (message == 'e') {
            valueE = LOW;
            led = "E=0";
            kaKuTransmitter.sendSignal('E', 1, false);

        /* ... */

        } else if (message == 'K') {
            valueK = HIGH;
            led = "K=1";
            kaKuTransmitter.sendSignal('A', 2, true);
        } else if (message == 'k') {
            valueK = LOW;
            led = "K=0";
            kaKuTransmitter.sendSignal('A', 2, false);
        } else if (message == 'L') {
            valueL = HIGH;
            led = "L=1";
            kaKuTransmitter.sendSignal('B', 2, true);
        } else if (message == 'l') {
            valueL = LOW;
            led = "L=0";
            kaKuTransmitter.sendSignal('B', 2, false);

        } else if (message == 'M') {
            valueM = HIGH;
            led = "M=1";
            kaKuTransmitter.sendSignal('C', 2, true);
        } else if (message == 'm') {
            valueM = LOW;
            led = "M=0";
            kaKuTransmitter.sendSignal('C', 2, false);

        } else if (message == 'N') {
            valueN = HIGH;
            led = "N=1";
            kaKuTransmitter.sendSignal('D', 2, true);
        } else if (message == 'n') {
            valueN = LOW;
            led = "N=0";
            kaKuTransmitter.sendSignal('D', 2, false);

        } else if (message == 'O') {
            valueO = HIGH;
            led = "O=1";
            kaKuTransmitter.sendSignal('E', 2, true);
        } else if (message == 'o') {
            valueO = LOW;
            led = "O=0";
            kaKuTransmitter.sendSignal('E', 2, false);

        } else if (message == 'X') {
            valueX = HIGH;
            led = "X=1";
            digitalWrite(pinX, valueX);
        } else if (message == 'x') {
            valueX = LOW;
            led = "X=0";
            digitalWrite(pinX, valueX);

        } else if (message == 'Y') {
            valueY = HIGH;
            led = "Y=1";
            digitalWrite(pinY, valueY);
        } else if (message == 'y') {
            valueY = LOW;
            led = "Y=0";
            digitalWrite(pinY, valueY);

        } else if (message == 'Z') {
            valueZ = HIGH;
            led = "Z=1";
            digitalWrite(pinZ, valueZ);
        } else if (message == 'z') {
            valueZ = LOW;
            led = "Z=0";
            digitalWrite(pinZ, valueZ);
        }
        Serial.println(led);
        Serial.flush();
        update = true;
    }

    if (update || ctr > 30) { // send data once every 3 seconds
        ctr = 0;

        // Read temperature
        int sensorVal = analogRead(sensorPin);
        float voltage = (sensorVal/1024.0) * 5.0;
        float temperature = (voltage - .5) * 100;

        // Create serial payload in the form: DATA:T=43.78;A=0;B=1;C=0;

        Serial.print("DATA:T1=");
        Serial.print(temperature);
        Serial.print(";A=");
        Serial.print(valueA);
        Serial.print(";B=");
        Serial.print(valueB);
        Serial.print(";C=");
        Serial.print(valueC);
        Serial.print(";D=");
        Serial.print(valueD);
        Serial.print(";E=");
        Serial.print(valueE);
        Serial.print(";K=");
        Serial.print(valueK);
        Serial.print(";L=");
        Serial.print(valueL);
        Serial.print(";M=");
        Serial.print(valueM);
        Serial.print(";N=");
        Serial.print(valueN);
        Serial.print(";O=");
        Serial.print(valueO);
        Serial.print(";X=");
        Serial.print(valueX);
        Serial.print(";Y=");
        Serial.print(valueY);
        Serial.print(";Z=");
        Serial.print(valueZ);
        Serial.println(";");
        Serial.flush();
    }

    if (update) { // blink after something happened
        digitalWrite(pinStatus, HIGH);
        delay(100);
        digitalWrite(pinStatus, LOW);
    } else {
        delay(100);
    }
    ctr++;
}


void blinkWhite() {
    digitalWrite(13, HIGH);
    delay(400);
    digitalWrite(13, LOW);
}


