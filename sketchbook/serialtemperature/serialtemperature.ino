
#include <RemoteTransmitter.h>


int sensorPin = A0;
const float baselineTemp = 20.0;

int pinA = 5;
int pinB = 4;
int pinC = 3;

int valueA = HIGH;
int valueB = HIGH;
int valueC = HIGH;

int message;
int ctr = 11;

// Intantiate a new KaKuTransmitter remote, also use pin 11
KaKuTransmitter kaKuTransmitter(11);

void setup() {
    Serial.begin(9600);

    for (int pinNumber = 3; pinNumber < 6; pinNumber++) {
        pinMode(pinNumber, OUTPUT);
        digitalWrite(pinNumber, HIGH);
    }
}

void loop() {
    if (Serial.available() > 0) {
        message = Serial.read();
        //Serial.flush();
        //Serial.println("ACK");
        //Serial.flush();
        Serial.print("LOG:LED switched ");
        String led = "";
        if (message == 'A') {
            digitalWrite(pinA, HIGH);
            valueA = HIGH;
            led = "Red=1";
            kaKuTransmitter.sendSignal('A', 1, true);
        } else if (message == 'a') {
            digitalWrite(pinA, LOW);
            valueA = LOW;
            led = "Red=0";
            kaKuTransmitter.sendSignal('A', 1, false);
        } else if (message == 'B') {
            digitalWrite(pinB, HIGH);
            valueB = HIGH;
            led = "Green=1";
        } else if (message == 'b') {
            digitalWrite(pinB, LOW);
            valueB = LOW;
            led = "Green=0";
        } else if (message == 'C') {
            digitalWrite(pinC, HIGH);
            valueC = HIGH;
            led = "Blue=1";
        } else if (message == 'c') {
            digitalWrite(pinC, LOW);
            valueC = LOW;
            led = "Blue=0";
        }
        Serial.println(led);
        Serial.flush();
        ctr = 11; // trigger send
    }
    delay(200);

    if (ctr > 10) {
        ctr = 0;

        int sensorVal = analogRead(sensorPin);
        float voltage = (sensorVal/1024.0) * 5.0;
        float temperature = (voltage - .5) * 100;

        // Data in the form: DATA:T=43.78;V=0.64;LED1=0;LED2=1;LED3=0;

        Serial.print("DATA:T=");
        Serial.print(temperature);
        Serial.print(";V=");
        Serial.print(voltage);
        Serial.print(";Red=");
        Serial.print(valueA);
        Serial.print(";Green=");
        Serial.print(valueB);
        Serial.print(";Blue=");
        Serial.print(valueC);
    //    Serial.print("");
        Serial.println(";");
        Serial.flush();
    }
    ctr++;
    delay(100);
}
